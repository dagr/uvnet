from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication, QMainWindow, QAction, QMessageBox, qApp
from PyQt5.QtSql import QSqlDatabase, QSqlQueryModel, QSqlTableModel
import sys, logging

class UVManagerException(Exception):
    pass

class UVManager(QMainWindow):
    def __init__(self, log):
        super(UVManager, self).__init__()        
        
        log.info('Loading GUI')

        self.ui = uic.loadUi('uvmanager.ui', self)

        log.info('Creating menus')

        menuFile = self.ui.menuBar.addMenu('&File')

        actionExit = QAction('E&xit', self)
        actionExit.setShortcut('Ctrl+Q')
        actionExit.setStatusTip('Exit application')
        actionExit.triggered.connect(self.quit)
        menuFile.addAction(actionExit)
        self.ui.toolBar.addAction(actionExit)

        log.info('Open database')

        db = QSqlDatabase.addDatabase('QODBC')
        db.setDatabaseName('Driver={SQL Server};Server=localhost,1433;Database=UVNET;Uid=uvnet_user;Pwd=uvnet_password;')
        if not db.open():
            raise UVManagerException(db.lastError().text())

        log.info('Creating models')

        self.productModel = QSqlTableModel()
        self.productModel.setTable('factortype')
        self.productModel.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.productModel.select()
        self.productModel.setHeaderData(0, Qt.Horizontal, "ID")
        self.productModel.setHeaderData(1, Qt.Horizontal, "Name")

        self.ui.tblProducts.setModel(self.productModel)

        self.productNameModel = QSqlQueryModel()
        self.productNameModel.setQuery('select name from factortype order by name desc')
        self.productNameModel.setHeaderData(0, Qt.Horizontal, "Name")

        self.ui.cboxFactorsProducts.setModel(self.productNameModel)

        log.info('Connecting signals')

        self.ui.btnFactorsSave.clicked.connect(self.printButtonPressed)

    def initialize(self, log):
        #db = QSqlDatabase.database()     
        pass

    def quit(self):
        qApp.quit()

    def printButtonPressed(self):
        self.btnFactorsSave.setText(self.ui.cboxFactorsProducts.currentText())

if __name__ == '__main__':
    try:        
        logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s')
        log = logging.getLogger('uvmanager_log')
        log.info('Starting application')

        app = QApplication(sys.argv)
        font = QFont('Consolas')
        font.setStyleHint(QFont.Monospace)
        app.setFont(font)

        uvm = UVManager(log)
        uvm.initialize(log)
        uvm.show()

        app.exec_()
        log.info('Exiting application')
    except UVManagerException as uvmex:
        log.error(str(uvmex))
    except Exception as ex:
        log.error(str(ex), exc_info=True)