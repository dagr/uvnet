# -*- coding: utf-8 -*-

import logging

def create_logger(name):
    ''' '''
    logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s')
    return logging.getLogger(name)