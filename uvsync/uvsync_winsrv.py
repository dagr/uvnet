# -*- coding: utf-8 -*-

import time, winreg, subprocess, servicemanager
from pathlib import Path
from winsrv import WinSrv

_registry_path = r"SOFTWARE\UVSync"

def set_registry_value(name, value):
    try:
        winreg.CreateKey(winreg.HKEY_LOCAL_MACHINE, _registry_path)
        registry_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, _registry_path, 0, winreg.KEY_WRITE)
        winreg.SetValueEx(registry_key, name, 0, winreg.REG_SZ, value)
        winreg.CloseKey(registry_key)
        return True
    except WindowsError:
        return False

def get_registry_value(name):
    try:
        registry_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, _registry_path, 0, winreg.KEY_READ)
        value, regtype = winreg.QueryValueEx(registry_key, name)
        winreg.CloseKey(registry_key)
        return value
    except WindowsError:
        return None

class UVSyncWinSrv(WinSrv):
    ''' '''
    _svc_name_ = "UVSync"
    _svc_display_name_ = "UVSync service"
    _svc_description_ = "Synchronization of UVNet station data"

    def start(self):
        ''' '''
        d = get_registry_value("bindir")
        if d is None:
            servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, 0xF000, ('UVSync: Unable to get registry key: bindir', ''))
            return

        self.bindir = Path(d)
        self.isrunning = True
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, 0xF000, ('UVSync starting', ''))

    def stop(self):
        ''' '''
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, 0xF000, ('UVSync stopping', ''))
        self.isrunning = False

    def main(self):
        ''' '''
        while self.isrunning:
            try:                
                uvsync = self.bindir / "uvsync.py"
                cp = subprocess.run(["python.exe", str(uvsync)])
                if cp.returncode != 0:
                    servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, 0xF000, ('UVSync error code: ' + cp.returncode, ''))
                else:
                    servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, 0xF000, ('UVSync success', ''))
            except Exception as ex:
                servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, 0xF000, ('UVSync failed: ' + str(ex), ''))
                
            time.sleep(10) # FIXME: get value from registry and calculate proper sleep time

if __name__ == '__main__':
    ''' '''
    UVSyncWinSrv.parse_command_line()