# -*- coding: utf-8 -*-

import sys, importlib
from datetime import date

class UVSyncContextException(Exception):
    ''' '''
    pass

class UVSyncContext():
    ''' '''
    def __init__(self, station, items, sync_date = date.today()):
        ''' '''
        self.station = station.strip()
        self.instrument_id = None
        self.fetch_module = None
        self.store_module = None        
        self.parameters = items
        self.sync_date = sync_date
        self.sync_files = []        
        
        if 'instrument_id' not in self.parameters:
            raise UVSyncContextException("Missing required parameter 'instrument_id' for station " + self.station)
        if 'fetch_module' not in self.parameters:
            raise UVSyncContextException("Missing required parameter 'fetch_module' for station " + self.station)
        if 'store_module' not in self.parameters:
            raise UVSyncContextException("Missing required parameter 'store_module' for station " + self.station) 

        if not self.parameters['instrument_id']:
            raise UVSyncContextException("Missing parameter value 'instrument_id' for station " + self.station)
        if not self.parameters['fetch_module']:
            raise UVSyncContextException("Missing parameter value 'fetch_module' for station " + self.station)
        if not self.parameters['store_module']:
            raise UVSyncContextException("Missing parameter value 'store_module' for station " + self.station)

        try:
            self.instrument_id = int(self.parameters['instrument_id'])
        except:
            raise UVSyncContextException("Invalid number format on instrument_id for station " + self.station)        

        self.fetch_module = self.get_module(self.parameters['fetch_module'].strip())
        if self.fetch_module is None:
            raise UVSyncContextException("Unable to import fetch module " + self.parameters['fetch_module'].strip() + " for station " + self.station)
                    
        self.store_module = self.get_module(self.parameters['store_module'].strip())
        if self.store_module is None:
            raise UVSyncContextException("Unable to import store module " + self.parameters['store_module'].strip() + " for station " + self.station)
                    
        reqparams = self.fetch_module.required_parameters()
        for p in reqparams:
            if p not in self.parameters:
                raise UVSyncContextException("Missing required fetch parameter " + p + " for station " + self.station)
            if not self.parameters[p]:
                raise UVSyncContextException("Missing value on fetch parameter " + p + " for station " + self.station)
        
        reqparams = self.store_module.required_parameters()
        for p in reqparams:
            if p not in self.parameters:
                raise UVSyncContextException("Missing required store parameter " + p + " for station " + self.station)
            if not self.parameters[p]:
                raise UVSyncContextException("Missing value on store parameter " + p + " for station " + self.station)
    
    def get_module(self, module_name):        
        ''' '''        
        return sys.modules[module_name] if module_name in sys.modules else importlib.import_module("lib." + module_name)