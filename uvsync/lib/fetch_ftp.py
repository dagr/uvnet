# -*- coding: utf-8 -*-

import logging

def required_parameters():
    ''' Returns a list of required parameters for the fetch function '''
    return [
        'fetch_host_name', 
        'fetch_host_port', 
        'fetch_directory_remote', 
        'fetch_directory_local', 
        'fetch_regexp'
        ]

def fetch(ctx):    
    ''' '''
    log = logging.getLogger("uvsync")

    log.info("Copying from FTP...")