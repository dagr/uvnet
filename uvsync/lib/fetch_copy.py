# -*- coding: utf-8 -*-

import logging
from pathlib import Path
from shutil import copy

_log = logging.getLogger("uvsync")

def required_parameters():
    ''' Returns a list of required parameters for the fetch function '''
    return [
        'fetch_directory_input', 
        'fetch_directory_output', 
        'fetch_match_expression'
        ]

def fetch(ctx):
    ''' 
    Copy files from argument 'fetch_directory_input' to 'fetch_directory_output' 
    filtered on strftime+fnmatch expression 'fetch_match_expression'
    '''    
    din = Path(ctx.parameters['fetch_directory_input'])
    dout = Path(ctx.parameters['fetch_directory_output'])
    expr = ctx.sync_date.strftime(ctx.parameters['fetch_match_expression'])

    _log.info("Processing directory " + str(din) + " with expression " + expr)
    
    ifiles = din.glob(expr)
    for fin in ifiles:
        try:                            
            fout = Path(dout) / fin.name
            _log.info("Copying from " + str(fin) + " to " + str(fout))
            copy(fin, fout)
            _log.info("Adding " + str(fout) + " to sync list")
            ctx.sync_files.append(fout)
        except IOError as ioerr:
            _log.error(str(ioerr))