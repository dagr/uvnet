# -*- coding: utf-8 -*-

import logging, datetime, pyodbc, csv

_log = logging.getLogger("uvsync")

def required_parameters():
    ''' Returns a list of required parameters for the store function '''
    return ['store_connection_string']

def store(ctx):
    ''' '''    
    connection_string = ctx.parameters['store_connection_string']

    _log.info("Using connection string [" + connection_string + "]")
    connection = None

    try:
        connection = pyodbc.connect(connection_string, autocommit = False)
        cursor = connection.cursor()
                                            
        cursor.execute("exec select_id_for_station ?", (ctx.station))
        row = cursor.fetchone()
        if not row:
            _log.error("Unable to find id for station " + ctx.station)
            return
        staid = row.id

        _log.info("Using station id: %d", (staid))

        last_dt = datetime.datetime(1970, 1, 1)
        cursor.execute("exec select_last_datetime_for_station ?", (ctx.station))
        row = cursor.fetchone()
        if row:
            last_dt = row.last_datetime

        _log.info("Using " + last_dt.strftime("%Y-%m-%d %H:%M:%S") + " as last datetime from DB")

        for file in ctx.sync_files:
            _log.info("Storing data from file " + str(file))                
            with file.open() as fd:            
                store_file(cursor, fd, staid, ctx.instrument_id, last_dt)

        connection.commit()

    except Exception as ex:
        if connection is not None:
            connection.rollback()            
        _log.error(str(ex), exc_info=True)    
    finally:
        if connection is not None:
            connection.close()

def store_file(cursor, fd, staid, instrid, last_dt):
    ''' '''
    new_measurement = old_measurement = invalid_measurement = 0
    line_count = 0

    csv_reader = csv.reader(fd, delimiter=',')

    for row in csv_reader:

        if line_count == 0:
            line_count += 1
            continue

        line_count += 1

        if len(row) != 30:
            _log.error("Invalid format on line number " + str(line_count))
            invalid_measurement += 1
            continue

        dt = datetime.datetime.strptime(row[2], "%Y-%m-%d %H:%M:%S")
        if dt <= last_dt:
            old_measurement += 1
            continue

        cursor.execute('exec insert_measurement19 ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?', (
            staid, instrid, dt,
            row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], 
            row[18], row[19], row[20], row[21], row[22], row[23], row[24], row[25], row[26]
        ))

        new_measurement += 1
    
    _log.info("A total of %d lines processed. %d lines was old, %d lines was invalid and %d lines was inserted" % (line_count-1, old_measurement, invalid_measurement, new_measurement))

