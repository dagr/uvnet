
=================================================
INSTALL

Installer Anaconda3 (Vi leser ikke fra .mdb filer så vi trenger ikke bruke Anaconda2)

Legg til Anaconda3 installasjonskatalog til PATH

Opprett registry key "HKEY_LOCAL_MACHINE\SOFTWARE\uvsync\bindir" og sett katalogen med uvsync.py som verdi

Åpne cmd som administrator og bruk følgende kommandoer for installasjon/oppdatering av windows service:

Install service: python uvsync_winsrv.py install
Update service: python uvsync_winsrv.py update
Remove service: python uvsync_winsrv.py remove

Kjør "eventvwr" for å åpne systemets event log

=================================================
CONFIG

uvsync.ini filen må være lagret med encoding ISO 8859-15 (ANSI)

Connection string skal typisk være: Driver={SQL Server};Server=localhost;Database=uvnet;UID=uvnet_user;PWD=uvnet_password;

    - localhost byttes ut med navn på SQL tjeneren

    - uvnet_user er en SQL bruker med login (mappet til windows bruker med samme navn), og har rollene 
        db_owner, db_reader og db_writer på databasen uvnet

=================================================
UVSYNC.PY

uvsync.py leser konfigurasjonsfilen uvsync.ini, henter data filer for hver stasjon og setter nye data inn i databasen.

Følgende kommandolinje parametere kan benyttes:

--config_dir ...

Dette er navn på katalogen som inneholder uvsync.ini filen. Bruker "working directory" som standard hvis ingenting er angitt.

--sync_date ...

Dette er hvilken dato som skal synkroniseres. Bruker dagens dato som standard hvis ingenting er angitt. 
Formatet er ISO 8601. Eksempel: 2019-03-23

--station ...

Dette er navn på stasjon som skal synkroniseres. Synkroniserer alle stasjoner hvis ingenting er angitt.

=================================================
UVSYNC.INI

uvsync.ini filen må være lagret med encoding ISO 8859-15 (ANSI)

Følgende parametere er påkrevet for hver stasjon:

    - instrument_id

        Heltall som angir stasjonens instrument ID

    - fetch_module
        
        Python modul som skal benyttes for henting av datafil

    - store_module    

        Python modul som skal benyttes for lesing av datafil og lagring i databasen


    Python modulen som er angitt i parameteret fetch_module har en funksjon (required_parameters) 
    som returnerer en liste med ytteligere påkrevde parametere, i tillegg til en funksjon for henting av datafil (fetch)

    Python modulen som er angitt i parameteret store_module har en funksjon (required_parameters) 
    som returnerer en liste med ytteligere påkrevde parametere, i tillegg til en funksjon for lagring av data (store)

=================================================