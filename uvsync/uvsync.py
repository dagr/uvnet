# -*- coding: utf-8 -*-

import sys, logging, argparse, configparser
import uvsync_log
from pathlib import Path
from datetime import date
from uvsync_context import UVSyncContextException, UVSyncContext

argparser = argparse.ArgumentParser()

argparser.add_argument('--config_dir', help='Set configuration directory. Default: Working directory')
argparser.add_argument('--sync_date', help='Set sync date. Default: Current date')
argparser.add_argument('--station', help='Set sync station. Default: All stations')

class UVSyncException(Exception):
    ''' '''
    pass

def main():
    ''' '''    
    log = uvsync_log.create_logger("uvsync")    

    try:
        args = argparser.parse_args()

        config_dir = Path(__file__).parent
        if args.config_dir:
            config_dir = Path(args.config_dir)
        
        sync_date = date.today()
        if args.sync_date:
            sync_date = date.fromisoformat(args.sync_date)

        config_file = config_dir / 'uvsync.ini'
        if not config_file.is_file():
            raise UVSyncException("Config file not found: " + str(config_file))

        log.info("Reading configuration file " + str(config_file))
        config = configparser.ConfigParser()
        config.read(config_file)

        sync_contexts = []
        
        for station in config.sections():
            if args.station and args.station != station:
                continue
                
            try:
                log.info("Creating sync context for station %s" % (station))
                ctx = UVSyncContext(station, config._sections[station], sync_date)
                sync_contexts.append(ctx)
            except UVSyncContextException as cex:
                log.error(str(cex))
            except Exception as ex:
                log.error(str(ex), exc_info=True)
                    
        for ctx in sync_contexts:
            log.info("Fetching data for station %s" % (ctx.station))
            ctx.fetch_module.fetch(ctx) # run in parallel
                       
        for ctx in sync_contexts:
            log.info("Storing data for station %s" % (ctx.station))
            ctx.store_module.store(ctx) # run in parallel
            
        log.info("Sync finished")

    except UVSyncException as uvex:
        log.error(str(uvex))
        return 1
    except Exception as ex:
        log.error(str(ex), exc_info=True)
        return 1
    
    return 0    
    
if __name__ == '__main__':
    ''' '''
    exit_status = main()
    sys.exit(exit_status)