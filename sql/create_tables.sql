use UVNET
go

/*=======================================================*/

create table factortype (
	id smallint identity(1,1) primary key,
	name nvarchar(32) not null
)
go

insert into factortype values('cie')
go

/*=======================================================*/

CREATE TABLE [dbo].[station2](
	[station_id] [smallint] NOT NULL,
	[station_name] [varchar](50) NOT NULL,
	[foldername] [varchar](255) NOT NULL,
	[latitude] [real] NOT NULL,
	[longitude] [real] NOT NULL,
 CONSTRAINT [station2$pk_station2] PRIMARY KEY CLUSTERED 
(
	[station_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

insert into station2 (station_id, station_name, foldername, latitude, longitude) 
select stasjon_id, stasjon, katalognavn, rlat, rlong from stasjon
go

exec sp_rename 'station2', 'station'
go

/*=======================================================*/

CREATE TABLE [dbo].[guvfactor](
	[id] [smallint] identity(1,1) primary key,
	[instrument_id] [smallint] NOT NULL,
	[factortype_id] [smallint] NOT NULL,
	[valid_from] [datetime] NOT NULL,
	[valid_to] [datetime] NOT NULL,
	[cie305] [real] NULL,
	[cie313] [real] NULL,
	[cie320] [real] NULL,
	[cie340] [real] NULL,
	[cie380] [real] NULL,
	[cie395] [real] NULL,
	[cie412] [real] NULL,
	[cie443] [real] NULL,
	[cie490] [real] NULL,
	[cie532] [real] NULL,
	[cie555] [real] NULL,
	[cie665] [real] NULL,
	[cie780] [real] NULL,
	[cie875] [real] NULL,
	[cie940] [real] NULL,
	[cie1020] [real] NULL,
	[cie1245] [real] NULL,
	[cie1640] [real] NULL,
	[par] [real] NULL 
)
GO

ALTER TABLE [dbo].[guvfactor]  WITH NOCHECK ADD  CONSTRAINT [guvfactor$fk_factortype_guvfactor] FOREIGN KEY([factortype_id])
REFERENCES [dbo].[factortype] ([id])
GO

ALTER TABLE [dbo].[guvfactor] CHECK CONSTRAINT [guvfactor$fk_factortype_guvfactor]
GO

declare @factortype_id int
select @factortype_id = id from factortype where name = 'cie'

insert into guvfactor (instrument_id, factortype_id, valid_from, valid_to, cie305, cie313, cie320, cie340, cie380, cie395, cie412, cie443, cie490, cie532, cie555, cie665, cie780, cie875, cie940, cie1020, cie1245, cie1640, par)
select distinct instrument_id, @factortype_id, DATETIMEFROMPARTS(1900, 1, 1, 0, 0, 0, 0 ), DATETIMEFROMPARTS(3000, 1, 1, 0, 0, 0, 0), cie305, cie313, cie320, cie340, cie380, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, par from guvciefactors
go

/*=======================================================*/

CREATE TABLE [dbo].[guvparam2](
	[station_id] [smallint] NOT NULL,
	[instrument_id] [smallint] NOT NULL,
	[measurement_date] [smalldatetime] NOT NULL,
	[d305] [real] NULL,
	[d313] [real] NULL,
	[d320] [real] NULL,
	[d340] [real] NULL,
	[d380] [real] NULL,
	[d395] [real] NULL,
	[d412] [real] NULL,
	[d443] [real] NULL,
	[d490] [real] NULL,
	[d532] [real] NULL,
	[d555] [real] NULL,
	[d665] [real] NULL,
	[d780] [real] NULL,
	[d875] [real] NULL,
	[d940] [real] NULL,
	[d1020] [real] NULL,
	[d1245] [real] NULL,
	[d1640] [real] NULL,
	[dpar] [real] NULL,
	[o305] [real] NULL,
	[o313] [real] NULL,
	[o320] [real] NULL,
	[o340] [real] NULL,
	[o380] [real] NULL,
	[o395] [real] NULL,
	[o412] [real] NULL,
	[o443] [real] NULL,
	[o490] [real] NULL,
	[o532] [real] NULL,
	[o555] [real] NULL,
	[o665] [real] NULL,
	[o780] [real] NULL,
	[o875] [real] NULL,
	[o940] [real] NULL,
	[o1020] [real] NULL,
	[o1245] [real] NULL,
	[o1640] [real] NULL,
	[opar] [real] NULL,
 CONSTRAINT [guvparam2$pk_guvparam2] PRIMARY KEY CLUSTERED 
(
	[station_id] ASC,
	[instrument_id] ASC,
	[measurement_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[guvparam2]  WITH NOCHECK ADD  CONSTRAINT [guvparam2$fk_station_guvparam2] FOREIGN KEY([station_id])
REFERENCES [dbo].[station] ([station_id])
GO

ALTER TABLE [dbo].[guvparam2] CHECK CONSTRAINT [guvparam2$fk_station_guvparam2]
GO

insert into guvparam2 (station_id, instrument_id, measurement_date, d305, d313, d320, d340, d380, d395, d412, d443, d490, d532, d555, d665, d780, d875, d940, d1020, d1245, d1640, dpar, o305, o313, o320, o340, o380, o395, o412, o443, o490, o532, o555, o665, o780, o875, o940, o1020, o1245, o1640, opar) 
select stasjon_id, instrument_id, datotid, d305, d313, d320, d340, d380, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, dpar, o305, o313, o320, o340, o380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, opar from guvparam
go

/*=======================================================*/

CREATE TABLE [dbo].[measurement2](
	[station_id] [smallint] NOT NULL,
	[instrument_id] [smallint] NOT NULL,
	[measurement_date] [smalldatetime] NOT NULL,
	[e305] [real] NULL,
	[e313] [real] NULL,
	[e320] [real] NULL,
	[e340] [real] NULL,
	[e380] [real] NULL,
	[e395] [real] NULL,
	[e412] [real] NULL,
	[e443] [real] NULL,
	[e490] [real] NULL,
	[e532] [real] NULL,
	[e555] [real] NULL,
	[e665] [real] NULL,
	[e780] [real] NULL,
	[e875] [real] NULL,
	[e940] [real] NULL,
	[e1020] [real] NULL,
	[e1245] [real] NULL,
	[e1640] [real] NULL,
	[par] [real] NULL,
	[dtemp] [real] NULL,
	[zenith] [real] NULL,
	[azimuth] [real] NULL,
 CONSTRAINT [measurement2$pk_measurement2] PRIMARY KEY CLUSTERED 
(
	[station_id] ASC,
	[instrument_id] ASC,
	[measurement_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

insert into measurement2 (station_id, instrument_id, measurement_date, e305, e313, e320, e340, e380, e395, e412, e443, e490, e532, e555, e665, e780, e875, e940, e1020, e1245, e1640, par, dtemp, zenith, azimuth) 
select stasjon_id, instrument_id, datotid, e305, e313, e320, e340, e380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, par, dtemp, 0, 0 from measurement
go

/*=======================================================*/

CREATE TABLE [dbo].[results](
	[station_id] [smallint] NOT NULL,
	[measurement_date] [smalldatetime] NOT NULL,
	[uvi9_11] [real] NULL,
	[uvi11_14] [real] NULL,
	[uvi14_17] [real] NULL,
	[uvi_max] [real] NULL,
	[med9_11] [real] NULL,
	[med11_14] [real] NULL,
	[med14_17] [real] NULL,
	[sum_cie] [real] NULL,
	[sum_e305] [real] NULL,
	[sum_e313] [real] NULL,
	[sum_e320] [real] NULL,
	[sum_e340] [real] NULL,
	[sum_e380] [real] NULL,
 CONSTRAINT [results$pk_uvi_med] PRIMARY KEY CLUSTERED 
(
	[station_id] ASC,
	[measurement_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[results]  WITH NOCHECK ADD  CONSTRAINT [results$fk_station_results] FOREIGN KEY([station_id])
REFERENCES [dbo].[station] ([station_id])
GO

ALTER TABLE [dbo].[results] CHECK CONSTRAINT [results$fk_station_results]
GO

insert into results (station_id, measurement_date, uvi9_11, uvi11_14, uvi14_17, uvi_max, med9_11, med11_14, med14_17, sum_cie, sum_e305, sum_e313, sum_e320, sum_e340, sum_e380) 
select stasjon_id, dato, uvi9_11, uvi11_14, uvi14_17, uvi_max, med9_11, med11_14, med14_17, sum_cie, sum_e305, sum_e313, sum_e320, sum_e340, sum_e380 from resultat
go

/*=======================================================*/

CREATE TABLE [dbo].[uvi_med2](
	[station_id] [smallint] NOT NULL,
	[measurement_date] [smalldatetime] NOT NULL,
	[uvi9_11] [real] NULL,
	[uvi11_14] [real] NULL,
	[uvi14_17] [real] NULL,
	[med9_11] [real] NULL,
	[med11_14] [real] NULL,
	[med14_17] [real] NULL,
	[uvimax] [real] NULL,
	[med09_17] [real] NULL,
	[med00_24] [real] NULL,
	[medest] [real] NULL,
 CONSTRAINT [uvi_med2$pk_uvi_med2] PRIMARY KEY CLUSTERED 
(
	[station_id] ASC,
	[measurement_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[uvi_med2]  WITH NOCHECK ADD  CONSTRAINT [uvi_med2$fk_stasjon_uvimed2] FOREIGN KEY([station_id])
REFERENCES [dbo].[station] ([station_id])
GO

ALTER TABLE [dbo].[uvi_med2] CHECK CONSTRAINT [uvi_med2$fk_stasjon_uvimed2]
GO

insert into uvi_med2 (station_id, measurement_date, uvi9_11, uvi11_14, uvi14_17, med9_11, med11_14, med14_17, uvimax, med09_17, med00_24, medest) 
select stasjon_id, dato, uvi9_11, uvi11_14, uvi14_17, med9_11, med11_14, med14_17, uvimax, med09_17, med00_24, medest from uvi_med
go

/*=======================================================*/

exec sp_rename 'uvi_med', 'uvi_med_old'
exec sp_rename 'uvi_med2', 'uvi_med'
drop table uvi_med_old
go

exec sp_rename 'guvparam', 'guvparam_old'
exec sp_rename 'guvparam2', 'guvparam'
drop table guvparam_old
go

exec sp_rename 'measurement', 'measurement_old'
exec sp_rename 'measurement2', 'measurement'
drop table measurement_old
go

drop table guvciefactors
go

drop table resultat
go

drop table stasjon
go