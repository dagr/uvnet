use UVNET
go

/*========== CREATE PROCEDURES ==========*/

create proc insert_measurement19
	@station_id smallint,
	@instrument_id smallint,
	@measurement_date smalldatetime,
    @e305 real,
    @e313 real,
    @e320 real,
    @e340 real,
    @e380 real,
    @e395 real,
    @e412 real,
    @e443 real,
    @e490 real,
    @e532 real,
    @e555 real,
    @e665 real,
    @e780 real,
    @e875 real,
    @e940 real,
    @e1020 real,
    @e1245 real,
    @e1640 real,
    @par real,
    @dtemp real,
	@zenith real,
	@azimuth real
as 
	insert into measurement values (
		@station_id,
		@instrument_id,
		@measurement_date,
		@e305,
		@e313,
		@e320,
		@e340,
		@e380,
		@e395,
		@e412,
		@e443,
		@e490,
		@e532,
		@e555,
		@e665,
		@e780,
		@e875,
		@e940,
		@e1020,
		@e1245,
		@e1640,
		@par,
		@dtemp,
		@zenith,
		@azimuth
	);
go

create proc insert_measurement6
	@station_id smallint,
	@instrument_id smallint,
	@measurement_date smalldatetime,
    @e305 real,
    @e313 real,
    @e320 real,
    @e340 real,
    @e380 real,
    @par real,
    @dtemp real
as 
	insert into measurement values (
		@station_id,
		@instrument_id,
		@measurement_date,
		@e305,
		@e313,
		@e320,
		@e340,
		@e380,
		0,0,0,0,0,0,0,0,0,0,0,0,0,
		@par,
		@dtemp,
		0,
		0
	);
go

create proc insert_station
	@station_id smallint,
    @station_name varchar(50),
    @foldername varchar(255),
    @latitude real,
    @longitude real
as 
	insert into station values (
		@station_id,
		@station_name,
		@foldername,
		@latitude,
		@longitude
	);
go

create proc insert_guvparam19
	@station_id smallint,
    @instrument_id smallint,
    @measurement_date smalldatetime,
    @d305 real,
    @d313 real,
    @d320 real,
    @d340 real,
    @d380 real,
    @d395 real,
    @d412 real,
    @d443 real,
    @d490 real,
    @d532 real,
    @d555 real,
    @d665 real,
    @d780 real,
    @d875 real,
    @d940 real,
    @d1020 real,
    @d1245 real,
    @d1640 real,
    @dpar real,
    @o305 real,
    @o313 real,
    @o320 real,
    @o340 real,
    @o380 real,
    @o395 real,
    @o412 real,
    @o443 real,
    @o490 real,
    @o532 real,
    @o555 real,
    @o665 real,
    @o780 real,
    @o875 real,
    @o940 real,
    @o1020 real,
    @o1245 real,
    @o1640 real,
    @opar real
as 
	insert into guvparam values (
		@station_id,
		@instrument_id,
		@measurement_date,
		@d305,
		@d313,
		@d320,
		@d340,
		@d380,
		@d395,
		@d412,
		@d443,
		@d490,
		@d532,
		@d555,
		@d665,
		@d780,
		@d875,
		@d940,
		@d1020,
		@d1245,
		@d1640,
		@dpar,
		@o305,
		@o313,
		@o320,
		@o340,
		@o380,
		@o395,
		@o412,
		@o443,
		@o490,
		@o532,
		@o555,
		@o665,
		@o780,
		@o875,
		@o940,
		@o1020,
		@o1245,
		@o1640,
		@opar
	);
go

create proc insert_guvparam6
	@station_id smallint,
    @instrument_id smallint,
    @measurement_date smalldatetime,
    @d305 real,
    @d313 real,
    @d320 real,
    @d340 real,
    @d380 real,    
    @dpar real,
    @o305 real,
    @o313 real,
    @o320 real,
    @o340 real,
    @o380 real,
    @opar real
as 
	insert into guvparam values (
		@station_id,
		@instrument_id,
		@measurement_date,
		@d305,
		@d313,
		@d320,
		@d340,
		@d380,
		1,1,1,1,1,1,1,1,1,1,1,1,1,
		@dpar,
		@o305,
		@o313,
		@o320,
		@o340,
		@o380,
		0,0,0,0,0,0,0,0,0,0,0,0,0,
		@opar
	);
go

create proc insert_guvfactor19
	@instrument_id smallint,
    @factortype_id int,
	@valid_from datetime,
	@valid_to datetime,
    @cie305 real,
    @cie313 real,
    @cie320 real,
    @cie340 real,
    @cie380 real,
    @cie395 real,
    @cie412 real,
    @cie443 real,
    @cie490 real,
    @cie532 real,
    @cie555 real,
    @cie665 real,
    @cie780 real,
    @cie875 real,
    @cie940 real,
    @cie1020 real,
    @cie1245 real,
    @cie1640 real,
    @par real
as 
	insert into guvfactor values (
		@instrument_id,
		@factortype_id,
		@valid_from,
		@valid_to,
		@cie305,
		@cie313,
		@cie320,
		@cie340,
		@cie380,
		@cie395,
		@cie412,
		@cie443,
		@cie490,
		@cie532,
		@cie555,
		@cie665,
		@cie780,
		@cie875,
		@cie940,
		@cie1020,
		@cie1245,
		@cie1640,
		@par
	);
go

create proc insert_guvfactor6
	@instrument_id smallint,
    @factortype_id int,
	@valid_from datetime,
	@valid_to datetime,
    @cie305 real,
    @cie313 real,
    @cie320 real,
    @cie340 real,
    @cie380 real,
    @par real
as 
	insert into guvfactor values (
		@instrument_id,
		@factortype_id,
		@valid_from,
		@valid_to,
		@cie305,
		@cie313,
		@cie320,
		@cie340,
		@cie380,
		1,1,1,1,1,1,1,1,1,1,1,1,1,
		@par
	);
go

create procedure ch_min6
    @station_name nvarchar(50),
	@factortype nvarchar(32),
    @date_from smalldatetime,
    @date_to smalldatetime
as 
   
DECLARE @station_id smallint
SELECT @station_id = station_id FROM dbo.station WHERE station_name = @station_name
         
DECLARE @factortype_id int
SELECT @factortype_id = id FROM dbo.factortype WHERE name = @factortype

SELECT 
	@station_name AS 'Stasjon', 
	CONVERT(varchar, m.measurement_date, 120) AS 'Datotid', 
	m.instrument_id AS 'Inst_id', 
	((m.e305 - p.o305) / p.d305) * f.cie305 + 
	((m.e313 - p.o313) / p.d313) * f.cie313 + 
	((m.e320 - p.o320) / p.d320) * f.cie320 + 
	((m.e340 - p.o340) / p.d340) * f.cie340 + 
	((m.e380 - p.o380) / p.d380) * f.cie380 AS 'Cie', 
	(m.e305 - p.o305) / p.d305 AS 'e305', 
	(m.e313 - p.o313) / p.d313 AS 'e313', 
	(m.e320 - p.o320) / p.d320 AS 'e320', 
	(m.e340 - p.o340) / p.d340 AS 'e340', 
	(m.e380 - p.o380) / p.d380 AS 'e380',
	p.measurement_date AS 'ParamDato'

FROM
	dbo.measurement as m, 
	dbo.guvfactor as f, 
	dbo.guvparam as p
WHERE 
	m.measurement_date >= @date_from AND 
	m.measurement_date < @date_to AND
	CAST(m.measurement_date AS date) = CAST(p.measurement_date AS date) AND
	m.station_id = @station_id AND 
	p.station_id = @station_id AND
	f.instrument_id = m.instrument_id AND
	f.factortype_id = @factortype_id AND f.valid_from <= m.measurement_date and f.valid_to > m.measurement_date and
	p.instrument_id = m.instrument_id
go 

create procedure ch_min19
    @station_name nvarchar(50),
	@factortype nvarchar(32),
    @date_from smalldatetime,
    @date_to smalldatetime
as 
   
DECLARE @station_id smallint
SELECT @station_id = station_id FROM dbo.station WHERE station_name = @station_name
         
DECLARE @factortype_id int
SELECT @factortype_id = id FROM dbo.factortype WHERE name = @factortype

SELECT 
	@station_name AS 'Stasjon', 
	CONVERT(varchar, m.measurement_date, 120) AS 'Datotid', 
	m.instrument_id AS 'Inst_id', 
	((m.e305 - p.o305) / p.d305) * f.cie305 + 
	((m.e313 - p.o313) / p.d313) * f.cie313 + 
	((m.e320 - p.o320) / p.d320) * f.cie320 + 
	((m.e340 - p.o340) / p.d340) * f.cie340 + 
	((m.e380 - p.o380) / p.d380) * f.cie380 +
	((m.e395 - p.o395) / p.d395) * f.cie395 +
	((m.e412 - p.o412) / p.d412) * f.cie412 +
	((m.e443 - p.o443) / p.d443) * f.cie443 +
	((m.e490 - p.o490) / p.d490) * f.cie490 +
	((m.e532 - p.o532) / p.d532) * f.cie532 +
	((m.e555 - p.o555) / p.d555) * f.cie555 +
	((m.e665 - p.o665) / p.d665) * f.cie665 +
	((m.e780 - p.o780) / p.d780) * f.cie780 +
	((m.e875 - p.o875) / p.d875) * f.cie875 +
	((m.e940 - p.o940) / p.d940) * f.cie940 +
	((m.e1020 - p.o1020) / p.d1020) * f.cie1020 +
	((m.e1245 - p.o1245) / p.d1245) * f.cie1245 +
	((m.e1640 - p.o1640) / p.d1640) * f.cie1640 AS 'Cie', 
	(m.e305 - p.o305) / p.d305 AS 'e305', 
	(m.e313 - p.o313) / p.d313 AS 'e313', 
	(m.e320 - p.o320) / p.d320 AS 'e320', 
	(m.e340 - p.o340) / p.d340 AS 'e340', 
	(m.e380 - p.o380) / p.d380 AS 'e380',
	(m.e395 - p.o395) / p.d395 AS 'e395',
	(m.e412 - p.o412) / p.d412 AS 'e412',
	(m.e443 - p.o443) / p.d443 AS 'e443',
	(m.e490 - p.o490) / p.d490 AS 'e490',
	(m.e532 - p.o532) / p.d532 AS 'e532',
	(m.e555 - p.o555) / p.d555 AS 'e555',
	(m.e665 - p.o665) / p.d665 AS 'e665',
	(m.e780 - p.o780) / p.d780 AS 'e780',
	(m.e875 - p.o875) / p.d875 AS 'e875',
	(m.e940 - p.o940) / p.d940 AS 'e940',
	(m.e1020 - p.o1020) / p.d1020 AS 'e1020',
	(m.e1245 - p.o1245) / p.d1245 AS 'e1245',
	(m.e1640 - p.o1640) / p.d1640 AS 'e1640',
	p.measurement_date AS 'ParamDato'	
FROM
	dbo.measurement as m, 
	dbo.guvfactor as f, 
	dbo.guvparam as p
WHERE 
	m.measurement_date >= @date_from AND 
	m.measurement_date < @date_to AND
	CAST(m.measurement_date AS date) = CAST(p.measurement_date AS date) AND
	m.station_id = @station_id AND 
	p.station_id = @station_id AND
	f.instrument_id = m.instrument_id AND
	f.factortype_id = @factortype_id AND f.valid_from <= m.measurement_date and f.valid_to > m.measurement_date and
	p.instrument_id = m.instrument_id
go 

create proc select_last_datetime_for_station	
	@station_name nvarchar(50)	
as
	declare @sid smallint
	select @sid = station_id from station where station_name = @station_name

	select max(measurement_date) as 'last_measurement_date' from measurement where station_id = @sid
go

create proc select_id_for_station	
	@station_name nvarchar(50)	
as	
	select station_id as 'id' from station where station_name = @station_name
go

/*========== ALTER PROCEDURES ==========*/

ALTER PROCEDURE [dbo].[cie_ch_min]  
   @stasjon varchar(15),
   @fra_datotid smalldatetime,
   @til_datotid smalldatetime
AS 
   
DECLARE @stasjon_id smallint
SELECT @stasjon_id = station_id FROM dbo.station WHERE station_name = @stasjon
         
DECLARE @factortype_id int
SELECT @factortype_id = id FROM dbo.factortype WHERE name = 'cie'

SELECT 
	@stasjon AS 'Stasjon', 
	CONVERT(char(8), m.measurement_date, 112) + ' ' + CONVERT(char(5), m.measurement_date, 8) AS 'Datotid', 
	m.instrument_id AS 'Inst_id', 
	((m.e305 - p.o305) / p.d305) * f.cie305 + ((m.e313 - p.o313) / p.d313) * f.cie313 + ((m.e320 - p.o320) / p.d320) * f.cie320 + ((m.e340 - p.o340) / p.d340) * f.cie340 + ((m.e380 - p.o380) / p.d380) * f.cie380 AS 'Cie', 
	(m.e305 - p.o305) / p.d305 AS 'e305', 
	(m.e313 - p.o313) / p.d313 AS 'e313', 
	(m.e320 - p.o320) / p.d320 AS 'e320', 
	(m.e340 - p.o340) / p.d340 AS 'e340', 
	(m.e380 - p.o380) / p.d380 AS 'e380',
	p.d305 AS 'd305',
	p.d313 AS 'd313',
	p.d320 AS 'd320',
	p.d340 AS 'd340',
	p.d380 AS 'd380',
	p.measurement_date AS 'ParamDato'
FROM 
	dbo.measurement AS m, 
	dbo.guvfactor  AS f, 
	dbo.guvparam  AS p
WHERE 
	m.measurement_date >= @fra_datotid AND 
	m.measurement_date < @til_datotid AND 
	CONVERT(date, p.measurement_date) = CONVERT(date, m.measurement_date) AND
	m.station_id = @stasjon_id AND 
	p.station_id = @stasjon_id AND 
	f.factortype_id = @factortype_id AND f.valid_from <= m.measurement_date and f.valid_to > m.measurement_date and
	f.instrument_id = m.instrument_id AND
	p.instrument_id = m.instrument_id AND
	CAST(m.measurement_date AS date) = CAST(p.measurement_date AS date)
go

ALTER PROCEDURE [dbo].[cie_min]  
   @stasjon varchar(15),
   @fra_datotid smalldatetime,
   @til_datotid smalldatetime
AS 
      
   DECLARE
      @instrument_id smallint
   DECLARE
      @stasjon_id smallint
      
	DECLARE @factortype_id int
	SELECT @factortype_id = id FROM dbo.factortype WHERE name = 'cie'

   SELECT @instrument_id = p.instrument_id, @stasjon_id = s.station_id
   FROM station AS s, dbo.guvparam AS p
   WHERE 
      s.station_name = @stasjon AND 
      s.station_id = p.station_id AND 
      p.measurement_date BETWEEN CONVERT(char(16), @fra_datotid, 103) AND CONVERT(char(16), @til_datotid,103)

   SELECT 
      CONVERT(char(2), (datepart(dd, m.measurement_date)), 3) + ' ' + 
      CONVERT(char(2), (datepart(mm, m.measurement_date)), 3) + ' ' + 
      CONVERT(char(4), (datepart(yy, m.measurement_date)), 3) + ' ' + 
      CONVERT(char(2), (datepart(hh, m.measurement_date)), 3) + ' ' + 
      CONVERT(char(2), (datepart(mi, m.measurement_date)), 3) AS 'Datotid', 
	  ((m.e305 - p.o305) / p.d305) * f.cie305 + ((m.e313 - p.o313) / p.d313) * f.cie313 + ((m.e320 - p.o320) / p.d320) * f.cie320 + ((m.e340 - p.o340) / p.d340) * f.cie340 + ((m.e380 - p.o380) / p.d380) * f.cie380 AS 'Cie'
   FROM dbo.measurement  AS m, dbo.guvfactor AS f, dbo.guvparam  AS p
   WHERE 
      m.measurement_date > @fra_datotid AND 
      m.measurement_date < @til_datotid AND 
      p.measurement_date > dateadd(hh, -24, @fra_datotid) AND 
      p.measurement_date <= @fra_datotid AND 
      m.station_id = @stasjon_id AND 
      p.station_id = @stasjon_id AND 
      f.factortype_id = @factortype_id AND f.valid_from <= m.measurement_date and f.valid_to > m.measurement_date and
      f.instrument_id = @instrument_id AND 
      m.instrument_id = @instrument_id AND 
      p.instrument_id = @instrument_id
go

ALTER PROCEDURE [dbo].[dagsverdier]  
   @dato varchar(12)
AS    
   SELECT 
      CONVERT(varchar(12), r.measurement_date, 103) AS 'Dato', 
      datepart(dy, r.measurement_date) AS 'Dagnr', 
      s.station_name, 
      round(r.uvi9_11, 1) AS 'uvi9_11', 
      round(r.uvi11_14, 1) AS 'uvi11_14', 
      round(r.uvi14_17, 1) AS 'uvi14_17', 
      round((r.uvi9_11 + r.uvi11_14 + r.uvi14_17) / 3, 1) AS 'Gj_snitt', 
      round(r.med9_11, 1) AS 'MED_09_11', 
      round(r.med11_14, 1) AS 'MED_11_14', 
      round(r.med14_17, 1) AS 'MED_14_17', 
      round(r.sum_cie / 210, 1) AS 'SUM_MED'
   FROM station AS s, dbo.results AS r
   WHERE s.station_id = r.station_id AND CONVERT(varchar(12), r.measurement_date, 103) = @dato
   ORDER BY s.latitude DESC
go

ALTER PROCEDURE [dbo].[maanedsoversikt_1]  
   @katalognavn varchar(15),
   @dato smalldatetime
AS 
     SELECT 
      (datepart(yy, b.measurement_date) * 100) + (datepart(mm, b.measurement_date)) AS '�r/m�ned', 
      count(*) AS 'Ant dager', 
      a.station_name AS 'Stasjon', 
      round(avg(b.uvi9_11), 1) AS 'UVindex 09-11', 
      round(avg(b.uvi11_14), 1) AS 'UVindex 11-14', 
      round(avg(b.uvi14_17), 1) AS 'UVindex 14-17', 
      round(((avg(b.uvi9_11) + avg(b.uvi11_14) + avg(b.uvi14_17)) / 3), 1) AS 'Gj_snitt', 
      round(sum(b.med9_11), 1) AS 'MED 09-11', 
      round(sum(b.med11_14), 1) AS 'MED 11-14', 
      round(sum(b.med14_17), 1) AS 'MED 14-17', 
      sum(round(b.sum_cie / 210, 1)) AS 'Sum MED'
   FROM station AS a, results AS b
   WHERE 
      a.station_id = b.station_id AND 
      a.foldername = @katalognavn AND 
      (datepart(yy, b.measurement_date) * 100) + (datepart(mm, b.measurement_date)) = (datepart(yy, @dato) * 100) + datepart(mm, @dato)
   GROUP BY (datepart(yy, b.measurement_date) * 100) + (datepart(mm, b.measurement_date)), a.foldername,station_name
   HAVING a.foldername = @katalognavn
go

ALTER PROCEDURE [dbo].[maanedsoversikt_2]  
   @katalognavn varchar(20),
   @fradato smalldatetime,
   @tildato smalldatetime
AS 
   SELECT (datepart(yy, b.measurement_date) * 100) + (datepart(mm, b.measurement_date)) AS '�r/m�ned', a.station_name, sum(b.sum_cie / 210) AS 'SUM_MED', count(*) AS 'Dager i m�neden'
   FROM station AS a, results AS b
   WHERE 
      b.measurement_date > @fradato AND 
      b.measurement_date < @tildato AND 
      a.station_id = b.station_id AND 
      a.foldername = @katalognavn
   GROUP BY  (datepart(yy, b.measurement_date) * 100) + (datepart(mm, b.measurement_date)),a.station_name,a.foldername
   HAVING a.foldername = @katalognavn
   order by (DATEPART (yy, b.measurement_date)*100)+(DATEPART(mm,b.measurement_date))
go

ALTER PROCEDURE [dbo].[rawdata_min]
   @stasjon varchar(15),
   @fra_datotid smalldatetime,
   @til_datotid smalldatetime
AS    
   DECLARE
      @instrument_id smallint
   DECLARE
      @stasjon_id smallint
   DECLARE @factortype_id int
	SELECT @factortype_id = id FROM dbo.factortype WHERE name = 'cie'

   SELECT @instrument_id = p.instrument_id, @stasjon_id = s.station_id
   FROM station AS s, guvparam AS p
   WHERE 
      s.station_name = @stasjon AND 
      s.station_id = p.station_id AND 
      p.measurement_date BETWEEN CONVERT(char(16), @fra_datotid, 3) AND CONVERT(char(16), @fra_datotid, 3)

   SELECT 
      @stasjon AS 'Stasjon', 
      CONVERT(char(8), m.measurement_date, 112) + ' ' + CONVERT(char(5), m.measurement_date, 8) AS 'Datotid', 
      @instrument_id AS 'Inst_id', 
      m.e305 AS 'e305', 
      m.e313 AS 'e313', 
      m.e320 AS 'e320', 
      m.e340 AS 'e340', 
      m.e380 AS 'e380', 
      f.cie305 AS 'cie305', 
      f.cie313 AS 'cie313', 
      f.cie320 AS 'cie320', 
      f.cie340 AS 'cie340', 
      f.cie380 AS 'cie380', 
      p.d305 AS 'd305', 
      p.d313 AS 'd313', 
      p.d320 AS 'd320', 
      p.d340 AS 'd340', 
      p.d380 AS 'd380', 
      p.o305 AS 'o305', 
      p.o313 AS 'o313', 
      p.o320 AS 'o320', 
      p.o340 AS 'o340', 
      p.o380 AS 'o380'
   FROM measurement AS m, guvfactor AS f, guvparam  AS p
   WHERE 
      m.measurement_date > @fra_datotid AND 
      m.measurement_date < @til_datotid AND 
      p.measurement_date > dateadd(hh, -24, @fra_datotid) AND 
      p.measurement_date <= @fra_datotid AND 
      m.station_id = @stasjon_id AND 
      p.station_id = @stasjon_id AND 
      f.factortype_id = @factortype_id AND f.valid_from <= m.measurement_date and f.valid_to > m.measurement_date and
      m.instrument_id = @instrument_id AND 
      p.instrument_id = @instrument_id
go

ALTER PROCEDURE [dbo].[ukeoversikt_1]  
   @katalognavn varchar(20),
   @fradato smalldatetime,
   @tildato smalldatetime
AS 
	SELECT  "�r/uke"=(datepart(yy, measurement_date) * 100) + (DATEpart(week,measurement_date)) , station_name, 
	"SUM_MED"= sum(sum_cie/210),"Dager i uken"=count(*)  from station a, results b 
	where measurement_date>@fradato and measurement_date < @tildato 
		and a.station_id=b.station_id 
		and foldername=@katalognavn 
	group by datepart(yy, b.measurement_date) * 100 + datepart(week,b.measurement_date),station_name,foldername
	having foldername=@katalognavn
	order by (DATEPART(yy,b.measurement_date)*100+DATEPART(WEEK,b.measurement_date))
go

ALTER PROCEDURE [dbo].[ukeoversikt_2]  
   @katalognavn varchar(15),
   @dato smalldatetime
AS 
   SELECT 
      (datepart(yy, b.measurement_date) * 100) + (datepart(week, b.measurement_date)) AS '�r/uke', 
      count(*), 
      a.station_name AS 'Stasjon', 
      round(avg(b.uvi9_11), 1) AS 'UVindex 09-11', 
      round(avg(b.uvi11_14), 1) AS 'UVindex 11-14', 
      round(avg(b.uvi14_17), 1) AS 'UVindex 14-17', 
      round(((avg(b.uvi9_11) + avg(b.uvi11_14) + avg(b.uvi14_17)) / 3), 1) AS 'Gj_snitt', 
      round(sum(b.med9_11), 1) AS 'MED 09-11', 
      round(sum(b.med11_14), 1) AS 'MED 11-14', 
      round(sum(b.med14_17), 1) AS 'MED 14-17', 
      sum(round(b.sum_cie / 210, 1)) AS 'Sum MED'
   FROM station AS a, results AS b
   WHERE 
      a.station_id = b.station_id AND 
      a.foldername = @katalognavn AND 
      (datepart(yy, b.measurement_date) * 100) + (datepart(week, b.measurement_date)) = (datepart(yy, @dato) * 100) + datepart(week, @dato)
   GROUP BY (datepart(yy, b.measurement_date) * 100) + (datepart(week, b.measurement_date)),foldername,a.station_name
   HAVING a.foldername = @katalognavn
go

ALTER PROCEDURE [dbo].[aarstrend_1]  
   @katalognavn varchar(20),
   @fradato smalldatetime,
   @tildato smalldatetime
AS    
   SELECT CONVERT(varchar(4), datepart(yy, b.measurement_date), 103) + CONVERT(varchar(3), datepart(dy, b.measurement_date), 103), a.station_name AS 'Stasjon', b.sum_cie / 210 AS 'SUM_MED'
   FROM station AS a, results AS b
   WHERE 
      b.measurement_date >= @fradato AND 
      a.station_id = b.station_id AND 
      a.foldername = @katalognavn
go

ALTER PROCEDURE [dbo].[aarstrend_2]  
   @katalognavn varchar(15),
   @dato smalldatetime
AS 
	SELECT 
		CONVERT(varchar(12), @dato, 103) AS 'Til og med dato',
		a.station_name AS 'Stasjon', 
		round(max(b.uvi_max), 1) AS 'UVindex max', 
		round(max(b.sum_cie / 210), 1) AS 'max MED for en dag', 
		round(sum(b.sum_cie / 210), 1) AS 'Sum MED'
	FROM station AS a, results AS b
	WHERE 
		a.station_id = b.station_id AND 
		a.foldername = @katalognavn AND 
		datepart(yy, b.measurement_date) = datepart(yy, @dato) AND 
		b.measurement_date <= @dato
	GROUP BY datepart(yy, b.measurement_date),a.foldername,a.station_name
	HAVING a.foldername = @katalognavn
	order by datepart(yy,b.measurement_date)
go

CREATE PROCEDURE [dbo].[temperatur_min]  
   @stasjon varchar(15),
   @fra_datotid smalldatetime,
   @til_datotid smalldatetime
AS 
	DECLARE @instrument_id smallint
	DECLARE @stasjon_id smallint

	SELECT @instrument_id = p.instrument_id, @stasjon_id = s.station_id
	FROM station AS s, guvparam AS p
	WHERE 
		s.station_name = @stasjon AND 
		s.station_id = p.station_id AND 
		p.measurement_date BETWEEN CONVERT(char(12), @fra_datotid, 1) AND CONVERT(char(12), @fra_datotid, 1)

	SELECT @stasjon AS 'Stasjon', 
		CONVERT(char(8), m.measurement_date, 112) + '' + CONVERT(char(5), m.measurement_date, 8) AS 'Datotid', 
		@instrument_id AS 'Inst_id', 
		m.dtemp AS 'Dtemp'
	FROM measurement AS m, guvfactor AS f, dbo.guvparam  AS p
	WHERE 
		m.measurement_date > @fra_datotid AND 
		m.measurement_date < @til_datotid AND 
		p.measurement_date > dateadd(hh, -24, @fra_datotid) AND 
		p.measurement_date <= @fra_datotid AND 
		m.station_id = @stasjon_id AND 
		p.station_id = @stasjon_id AND 
		f.instrument_id = @instrument_id AND 
		m.instrument_id = @instrument_id AND 
		p.instrument_id = @instrument_id
go

DROP PROCEDURE IF EXISTS dbo.cie_ch_min_bjorn;  
GO  

DROP PROCEDURE IF EXISTS dbo.cie_ch_min_test;
GO  

DROP PROCEDURE IF EXISTS dbo.cie_min_bjorn;  
GO  

DROP PROCEDURE IF EXISTS dbo.cie_min_Fredrik;  
GO  

DROP PROCEDURE IF EXISTS dbo.rawdata_min_bjorn;  
GO  

DROP PROCEDURE IF EXISTS dbo.rawdata_min_test;  
GO  

DROP PROCEDURE IF EXISTS dbo.test;  
GO  

DROP PROCEDURE IF EXISTS dbo.test2;  
GO  

DROP PROCEDURE IF EXISTS labview.temperatur_min;  
GO  

DROP PROCEDURE IF EXISTS labview.temperatur_min_bjorn;  
GO  

alter view doytable
as
SELECT CONVERT(varchar(12), r.measurement_date, 103) AS Dato, DATEPART(dy, r.measurement_date) AS Dagnr, s.station_name, ROUND(r.uvi9_11, 1) AS uvi9_11, ROUND(r.uvi11_14, 1) AS uvi11_14, 
	ROUND(r.uvi14_17, 1) AS uvi14_17, ROUND((r.uvi9_11 + r.uvi11_14 + r.uvi14_17) / 3, 1) AS Gj_snitt, ROUND(r.med9_11, 1) AS MED_09_11, ROUND(r.med11_14, 1) AS MED_11_14, ROUND(r.med14_17, 1) 
    AS MED_14_17, ROUND(r.med9_11 + r.med11_14 + r.med14_17, 1) AS SUM_MED
FROM station AS s INNER JOIN
     dbo.results AS r ON s.station_id = r.station_id
GO

ALTER VIEW uvi_max
AS
SELECT s.station_name, s.foldername, r.uvi_max AS UV_index_max, s.latitude
FROM station AS s INNER JOIN
     results AS r ON s.station_id = r.station_id
WHERE ((DATEPART(dd, r.measurement_date) + DATEPART(mm, r.measurement_date) * 100) + DATEPART(yy, r.measurement_date) * 10000 = (DATEPART(dd, GETDATE()) + DATEPART(mm, GETDATE()) * 100) 
    + DATEPART(yy, GETDATE()) * 10000)
GO
