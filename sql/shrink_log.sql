use UVNET;
go

/*
DECLARE @path NVARCHAR(255) = N'C:\DB\Backup\uvnet_backup_' 
  + CONVERT(CHAR(8), GETDATE(), 112) + '_'
  + REPLACE(CONVERT(CHAR(8), GETDATE(), 108),':','')
  + '.trn';

BACKUP LOG UVNET TO DISK = @path WITH INIT, COMPRESSION;
*/

/*
checkpoint;
go

checkpoint;
go

dbcc SHRINKFILE(UVNET_log, 600);
go
*/